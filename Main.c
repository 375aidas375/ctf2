#if defined _WIN32
#define _WIN32_WINNT 0x0501
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <io.h>
#else
#define closesocket close
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <unistd.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define DEFAULT_BUFLEN 4048

void ClearWin()
{
#if defined _WIN32
    WSACleanup();
#endif
}

int login(int *Socketas, char *host_addr, int port);

int main()
{
#if defined _WIN32
    WSADATA wsaData;
    if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
    {
        printf("WSAStartup nepasileido su klaida\n");
        return 0;
    }
#endif

    int ConnectSocket;

    int def_port = 5555;
    char *addr = "78.56.77.230"; // serverio IPv4 adresas

    char msg[DEFAULT_BUFLEN];
    char response[DEFAULT_BUFLEN];

    if (login(&ConnectSocket, addr, def_port) == 0){
        closesocket(ConnectSocket);
        ClearWin();
        return 0;
    }

    FILE *fp;   //Atidaro faila ir suranda jo dydi
    fp = fopen("text.txt", "r");
    long int size;
    fseek(fp, 0, SEEK_END);
    size = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    printf("Failas: %ld\n", size);
    fgets(msg, size, (FILE*)fp);

    int sent = 0;
    int bytes;
    int sending = 12;
    
    memset(response,0,sizeof(response));

    do{
    	bytes = send(ConnectSocket, msg+sent, sending, 0);
    	recv(ConnectSocket, response+sent, sending, 0);
    	if (bytes < 0)
    	{
    		printf("error");
    		break;
    	}
    	if (bytes == 0)
    		break;
    	sent += bytes;
    	if(sent+sending > size){
    		sending = size - sent;
    	}
    } while (sent < size);
    
	printf("Sent: %d\n", sent);
	        
    fclose(fp);
       
    fp = fopen("Result.txt", "w+"); // Sukuria isvesties faila ir iraso atsakima
    fprintf(fp, "%s", response);   


    fclose(fp);    
    closesocket(ConnectSocket);
    ClearWin();
    return 0;
}

int login(int *Socketas, char *host_addr, int port)
{
    struct hostent *rem;
    struct in_addr addr;

    addr.s_addr = inet_addr(host_addr);
    rem = gethostbyaddr((char *)&addr, 4, AF_INET);

    struct sockaddr_in sin;
    memset(&sin, 0, sizeof(sin));
    sin.sin_port = htons(port);
    sin.sin_family = AF_INET;
    memcpy(&sin.sin_addr, rem->h_addr_list[0], sizeof(sin.sin_addr));

    *Socketas = socket(AF_INET, SOCK_STREAM, 0);

    if (connect(*Socketas, (struct sockaddr *)&sin, sizeof(sin)) < 0)
    {
        printf("Prisijungti nepaviko");
        closesocket(*Socketas);
        ClearWin();
        return 0;
    }
    printf("connected\n");
    return 1;
}
