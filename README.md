# Programos naudijimas

Programa automatiškai prisijungia prie serverio

Išsiunčiamo failo pavadinimas turi būti: "text.txt"
Ir turi buti toje pačioje direktorijoje kaip ir programa.

Gautas rezultatas bus įrašytas į "Rezult.txt" failą

## Programos paleidimas

Windowsams naudoti komanda:

```
gcc Main.c -o Main -lws2_32 && Main
```

Linux naudoti komanda:

```
gcc Main.c -o Main && ./Main
```
